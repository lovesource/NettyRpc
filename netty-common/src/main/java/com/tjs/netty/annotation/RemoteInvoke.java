package com.tjs.netty.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * 
* @ClassName: RemoteInvoke
* @Description: 给方法消费者程序用的
* @ClassName: RemoteInvoke
* @<p>Company: www.***.com</p>
* @author @author taojinsen
* @date 2018年5月16日 上午8:26:19
* @version 1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface RemoteInvoke {
}
