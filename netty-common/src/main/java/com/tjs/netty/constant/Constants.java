package com.tjs.netty.constant;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Constants {

	
	/**
	 * netty 服务器 注册到 zookeeper的nettyServer工作空间下
	 */
	public static String WORK_SPACE = "nettyServer";

	/**
	 * netty服务地址
	 */
	public static String HOST = null;
	/**
	 * netty服务端口
	 */
	public static int PORT = 8080;
	
	/**
	 * netty 权重
	 */
	public static int WEIGHT = 1;
	/**
	 * 随机还是权重	
	 */
	public static int TYPE = 1;// 1:随机 ; 2:加权   
	
	
	static {
		try {
			HOST = InetAddress.getLocalHost().getHostAddress() ;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 客户端60s没有读的话就进入ServerHandler的userEventTriggered读空闲
	 */
	public static int readerIdleTimeSeconds = 600 ;
	/**
	 * 客户端45s没有写的话就进入ServerHandler的userEventTriggered写空闲
	 */
	public static int writerIdleTimeSeconds = 450;
	/**
	 * 客户端20s没有读写的话就进入ServerHandler的userEventTriggered读写空闲
	 */
	public static int allIdleTimeSeconds = 200 ;
	
	
	public static final String AttributeKey = "AttributeKey";
	
	public enum Type {
	     RANDOM("随机", 1), WEIGHT("加权", 2);
	     
	    private String name ;
	    private int index ;
	     
	    private Type( String name , int index ){
	        this.name = name ;
	        this.index = index ;
	    }
	     
	    public String getName() {
	        return name;
	    }
	    public void setName(String name) {
	        this.name = name;
	    }
	    public int getIndex() {
	        return index;
	    }
	    public void setIndex(int index) {
	        this.index = index;
	    }
	}
	
	
}
