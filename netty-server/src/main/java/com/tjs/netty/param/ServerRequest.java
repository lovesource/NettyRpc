package com.tjs.netty.param;

public class ServerRequest {

	private Long id;   //唯一id
	private Object content; //内容
	private String command;// 类名全路径.方法名  eg: com.tjs.user.controller.UserController.test03

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "ServerRequest [id=" + id + ", content=" + content + ", command=" + command + "]";
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
	
}
