package com.tjs.netty.medium;

import java.lang.reflect.Method;

public class BeanMethod {
	
	/**
	 * 方法 类似
	 *  public void com.tjs.user.controller.UserController.test02()
	 */
	private Method method;
	/**
	 * 实例化的bean
	 */
	private Object bean;
	
	
	public BeanMethod() {
	}
	
	public BeanMethod(Method method, Object bean) {
		this.method = method;
		this.bean = bean;
	}
	
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}
	public Object getBean() {
		return bean;
	}
	public void setBean(Object bean) {
		this.bean = bean;
	}

	@Override
	public String toString() {
		return "BeanMethod [method=" + method + ", bean=" + bean + "]";
	}
}
