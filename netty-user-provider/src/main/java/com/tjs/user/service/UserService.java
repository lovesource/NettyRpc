package com.tjs.user.service;

import org.springframework.stereotype.Service;

import com.tjs.user.bean.User;

@Service
public class UserService {
	
	public User saveUser(User user) {
		user.setId(1);
		user.setName("UserService saveUser");
		
		return user;
	}
	
	public User findUser(String name) {
		return new User(1, "UserService  findUser");
	}

	
}
